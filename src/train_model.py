import torch
import torch.optim as optim
from pytorch_metric_learning import losses
from pytorch_metric_learning.utils import accuracy_calculator as ac
from torchvision import transforms

from src.data.dataset import get_dataloader
# from src.predict_model import test
from src.models.model import get_model

# import timm


device = 'cpu'
num_epochs = 2


def train(model,
          loss_func,
          device,
          train_loader,
          optimizer,
          loss_optimizer,
          epoch
          ):
    model.train()
    for batch_idx, (data, labels) in enumerate(train_loader):
        data, labels = data.to(device), labels.to(device)
        optimizer.zero_grad()
        loss_optimizer.zero_grad()
        embeddings = model(data)
        loss = loss_func(embeddings, labels)
        loss.backward()
        optimizer.step()
        loss_optimizer.step()
        if batch_idx % 100 == 0:
            print(f"Epoch {epoch} Iteration {batch_idx}: Loss = {loss}")


model = get_model().to(device)
config: dict = {}
loader = get_dataloader(config)
# {'train_loader': train_loader, 'test_loader': test_loader}
train_loader = loader['train_loader']
dataset_train = loader['dataset_train']
dataset_test = loader['dataset_test']

img_mean, img_std = (0.1307,), (0.3081,)
transform = transforms.Compose(
    [transforms.ToTensor(), transforms.Normalize(img_mean, img_std)]
)

optimizer = optim.Adam(model.parameters(), lr=0.01)
loss_func = losses.SubCenterArcFaceLoss(num_classes=10, embedding_size=128)
loss_func.to(device)
loss_optimizer = torch.optim.Adam(loss_func.parameters(), lr=1e-4)
accuracy_calculator = ac.AccuracyCalculator(include=("precision_at_1",), k=1)

for epoch in range(1, num_epochs + 1):
    train(model,
          loss_func,
          device,
          train_loader,
          optimizer,
          loss_optimizer,
          epoch
          )
    # test(dataset_train, dataset_test, model, accuracy_calculator)
