# -*- coding: utf-8 -*-
import logging
from pathlib import Path
import random
import os

import click
import cv2
from dotenv import find_dotenv, load_dotenv
import pandas as pd
from tqdm import tqdm


def is_img_avaible(img_path):
    img = cv2.imread(img_path)
    if img is not None:
        return True
    return False


@click.command()
@click.argument('input_folderpath', type=click.Path(exists=True))
@click.argument('output_train_filepath', type=click.Path())
@click.argument('output_val_filepath', type=click.Path())
@click.argument('output_test_filepath', type=click.Path())
def main(input_folderpath,
         output_train_filepath,
         output_val_filepath,
         output_test_filepath
         ):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')
    random.seed(10)
    uniq_clusters = os.listdir(input_folderpath)
    random.shuffle(uniq_clusters)
    persent80 = int(len(uniq_clusters) * 0.80)
    persent95 = int(len(uniq_clusters) * 0.95)

    paths = []
    clustes_of_face = []
    for i, cluster in tqdm(enumerate(uniq_clusters)):
        cluster_path = input_folderpath / Path(cluster)
        imgs_names = os.listdir(cluster_path)
        img_paths = []
        for img_name in imgs_names:
            img_path = str(cluster_path / img_name)
            if is_img_avaible(img_path):
                img_paths += [str(cluster_path / img_name)]
        if len(img_paths) > 0:
            paths.extend(img_paths)
            clustes_of_face.extend([i] * len(img_paths))
        if i == persent80:
            df = pd.DataFrame(list(zip(paths, clustes_of_face)),
                              columns=['path', 'cluster'])
            df.to_csv(output_train_filepath)
            paths = []
            clustes_of_face = []
        if i == persent95:
            df = pd.DataFrame(list(zip(paths, clustes_of_face)),
                              columns=['path', 'cluster'])
            df.to_csv(output_val_filepath)
            paths = []
            clustes_of_face = []

    df = pd.DataFrame(list(zip(paths, clustes_of_face)),
                      columns=['path', 'cluster'])
    df.to_csv(output_test_filepath)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
