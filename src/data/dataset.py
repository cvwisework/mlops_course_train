# -*- coding: utf-8 -*-
import cv2
import numpy as np
import pandas as pd
import torch
from torch.utils.data import Dataset


class FaceDataset(Dataset):
    def __init__(self, csv_file, train=True, transform=None):
        super().__init__()
        self.data = pd.read_csv(csv_file)
        self.transform = transform
        self.train = train

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, index):
        if self.train:
            img_path = self.data.iloc[index]['path']
            image = cv2.imread(img_path)
            labels = self.data.iloc[index]['class']
        else:
            img_path = self.data.iloc[index]['path']
            image = cv2.imread(img_path)
            labels = None

        if self.transform:
            augmentations = self.transform(image=image)
            image = augmentations["image"]

        return image, labels.astype(np.float32)


def get_dataloader(config):
    batch_size = config.batch_size
    transform_train = config.train.transform
    transform_test = config.test.transform
    csv_file_train = config.train.csv_file
    csv_file_test = config.test.csv_file

    dataset_train = FaceDataset(csv_file_train,
                                train=True,
                                transform=transform_train
                                )
    dataset_test = FaceDataset(csv_file_test,
                               train=False,
                               transform=transform_test
                               )
    train_loader = torch.utils.data.DataLoader(dataset_train,
                                               batch_size=batch_size,
                                               shuffle=True
                                               )
    test_loader = torch.utils.data.DataLoader(dataset_test, batch_size=batch_size)
    return {'train_loader': train_loader, 'test_loader': test_loader,
            'dataset_train': dataset_train,
            'dataset_test': dataset_test}
